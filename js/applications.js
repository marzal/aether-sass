/**
 * SPDX-FileCopyrightText: (c) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Swiper from 'swiper';
import '../css/kde-org/applications.scss';

let swiper = new Swiper('.swiper-container', {
  slidesPerView: 'auto',
  spaceBetween: 30,

  // If we need pagination
  pagination: {
    el: '.swiper-pagination',
    spaceBetween: 30,
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

const search = document.getElementById('search');
if (search) {
  search.addEventListener('input', function(e) {
    const apps = document.querySelectorAll('.app');
    const regex = new RegExp(e.target.value, 'i');
    apps.forEach(function(app) {
      const imgTitleContainsRegex = app.querySelector('img').title.search(regex) !== -1;
      const descriptionContainsRegex = app.querySelector('p').innerText.search(regex) !== -1;

      if (imgTitleContainsRegex || descriptionContainsRegex) {
        // app name is in search
        if (app.classList.contains('d-none')) {
          app.classList.remove('d-none');
        }
      } else if (!app.classList.contains('d-none')) {
        app.classList.add('d-none');
      }
    });

    const categories = document.querySelectorAll('.category');
    categories.forEach(function(category) {
      const hasVisibleApps = category.querySelectorAll('.applications .app:not(.d-none)').length > 0;
      const hasVisibleAddons = category.querySelectorAll('.addons .app:not(.d-none)').length > 0;

      const appsTitle = category.querySelector('.applications-title');
      if (appsTitle) {
        if (hasVisibleApps && appsTitle.classList.contains('d-none')) {
          appsTitle.classList.remove('d-none');
        } else if (!hasVisibleApps && !appsTitle.classList.contains('d-none')) {
          appsTitle.classList.add('d-none');
        }
      }

      const addonsTitle = category.querySelector('.addons-title');
      if (addonsTitle) {
        if (addonsTitle && hasVisibleAddons && addonsTitle.classList.contains('d-none')) {
          addonsTitle.classList.remove('d-none');
        } else if (addonsTitle && !hasVisibleAddons && !addonsTitle.classList.contains('d-none')) {
          addonsTitle.classList.add('d-none');
        }
      }

      if ((hasVisibleApps || hasVisibleAddons) && category.classList.contains('d-none')) {
        category.classList.remove('d-none');
      } else if (!(hasVisibleApps || hasVisibleAddons) && !category.classList.contains('d-none')) {
        category.classList.add('d-none');
      }
    });
  });
}

const unmaintainedCollapsible = document.getElementById('unmaintained-collapse-link');
if (unmaintainedCollapsible) {
  var observer = new MutationObserver(function(mutations) {
    for (let mutation of mutations) {
      if (mutation.type === 'attributes') {
        const icon = unmaintainedCollapsible.querySelector('i');
        if (!mutation.oldValue) {
          icon.classList.add('icon_expand-all');
          icon.classList.remove('icon_collapse-all');
        } else if (mutation.oldValue === "collapsed") {
          icon.classList.add('icon_collapse-all');
          icon.classList.remove('icon_expand-all');
        }
      }
    }
  });

  observer.observe(unmaintainedCollapsible, {
    attributes: true,
    attributeFilter: ['class'],
    attributeOldValue: true,
    childList: false,
    characterData: false
  });
}
